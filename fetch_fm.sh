cd web/modules/contrib
git clone git@git.drupal.org:project/form_mode_manager.git
cd form_mode_manager
git checkout 8.x-2.x
git remote add form_mode_manager-3164478 https://git.drupalcode.org/issue/form_mode_manager-3164478.git
git fetch form_mode_manager-3164478
git checkout 3164478-drupal-9-deprecated
cd ../../..
ddev drush pm:enable form_mode_manager form_mode_user_roles_assign
