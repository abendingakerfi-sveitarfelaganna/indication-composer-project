# Composer project

Stores the foundation of the indication project.

## Local development
Follow the the steps to set up a local installation (see "Usage" section for composer
and git requirements, also make sure ddev is installed):

1. Create a folder for your project in your workspace directory and then enter that directory.

2. Create a basic ddev config:

```
ddev config --project-type drupal9 --docroot . --project-name="PROJECT_NAME"
```

3. Start up DDEV and then enter the web container:

```
ddev start && ddev ssh
```

4. Use Composer (within the DDEV web container) to create the composer project based on the Um að gera profile:

```
composer create-project --stability dev abendingakerfi-sveitarfelaganna/indication-composer-project PROJECT_NAME --remove-vcs --ignore-platform-reqs --repository-url="{\"type\": \"vcs\",\"url\":  \"git@gitlab.com:abendingakerfi-sveitarfelaganna/indication-composer-project.git\"}"
```
(Split line for easier copying):

```
composer create-project --stability dev abendingakerfi-sveitarfelaganna/indication-composer-project PROJECT_NAME
```

```
--remove-vcs --ignore-platform-reqs --repository-url="{\"type\": \"vcs\",\"url\":  \"git@gitlab.com:abendingakerfi-sveitarfelaganna/indication-composer-project.git\"}"
```

This will install a new project using the new Um að gera profile.

5. Exit the web container: `exit`

6. Next you need to move the project files "down one folder" (you might get an error, but you can ignore it):

```
mv PROJECT_NAME/* .
mv PROJECT_NAME/.* .

```

7. Set the new docroot and restart the project:

```
ddev config --docroot web && ddev restart

```

8. And now you can install the project with the profile:

```
ddev drush si indication_profile --site-name=PROJECT_NAME

```
