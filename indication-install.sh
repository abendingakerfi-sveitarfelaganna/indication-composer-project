#!/bin/bash
read -p "Enter project name: "  name
export projectname="`echo "${name}"`"

echo "Moving project folders up one level"
mv $projectname/* .
mv $projectname/.* .
rm -rf $projectname

echo "Removing the sites folder that was created by DDEV, since we have all that in the composer project."
rm -rf sites

ddev config --php-version="8.0" --mariadb-version="10.4" --docroot="web"
ddev restart 

echo "ssh auth..."

ddev auth ssh

echo "Enabling uag drush commands"
mv .ddev/commands/web/drush.example .ddev/commands/web/drush

echo "Installing site: $projectname"
ddev drush si indication_profile --site-name="${name}"
ddev drush pm:enable indication
./fetch_fm.sh